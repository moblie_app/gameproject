import 'dart:io';

class Player {
  String name = "pa";
  int score = 0;
  int r = 0;
  int c = 0;

  void showPlayer(int r, int c, String name, int score) {
    name = name;
    score = score;
    r = r;
    c = c;
  }

  String toString() {
    return "Player is at position ($r, $c)";
  }
}