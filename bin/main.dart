import 'dart:io';

class Map {
  int r;
  int c;
  String symbol;

  Map(this.r, this.c, this.symbol);

  bool stayOn(int r, int c) {
    return this.r == r && this.c == c;
  }
}

class Player extends Map {
  Game game;
  late int r;
  late int c;
  String symbol = 'P';
  int score = 0;
  int countQ = 1;
  Player(this.r, this.c, this.game) : super(r, c, "P");

  bool walk(String direction) {
    switch (direction) {
      case 'N':
      case 'a':
        if (walkN()) return false;
        break;
      case 'S':
      case 'd':
        if (walkS()) return false;
        break;
      case 'E':
      case 's':
        if (walkE()) return false;

        break;
      case 'W':
      case 'w':
        if (walkW()) return false;
        break;
      default:
        return false;
    }
    checkBomb();
    checkQuestion();
    return true;
  }

  void checkBomb() {
    if (game.isBomb(r, c)) {
      game.showMap();
      print("Found Bomb");
      print("You lose.");
      game.isFinished = true;
    }
  }

  void checkQuestion() {
    String answer;

    if (game.isQuestion(r, c)) {
      if (countQ == 1) {
        print("กรุณาเติมตัวอักษรที่หายไป");
        print("Do_phin");
        answer = stdin.readLineSync()!;
        while (answer != 'l') {
          print("กรุณาเติมตัวอักษรที่หายไป");
          print("Do_phin");
          answer = stdin.readLineSync()!;
        }
        score++;
        print("Score = $score");
        if (score == 2) {
          game.showMap();
          print('You Win!!!');
          game.isFinished = true;
        }
      }
    }

    // ---------------------------------

    if (game.isQuestion(r, c)) {
      countQ++;
      if (countQ == 3) {
        print("กรุณาเติมตัวอักษรที่หายไป");
        print("Capy_ara");
        answer = stdin.readLineSync()!;
        while (answer != 'b') {
          print("กรุณาเติมตัวอักษรที่หายไป");
          print("Capy_ara");
          answer = stdin.readLineSync()!;
        }
        score++;
        print("Score = $score");
        if (score == 2) {
          game.showMap();
          print('You Win!!!');
          game.isFinished = true;
        }
      }
    }
  }

  bool canWalk(int r, int c) {
    return game.inMap(r, c);
  }

  bool walkW() {
    if (canWalk(r - 1, c)) {
      r = r - 1;
    } else {
      return true;
    }
    return false;
  }

  bool walkE() {
    if (canWalk(r + 1, c)) {
      r = r + 1;
    } else {
      return true;
    }
    return false;
  }

  bool walkS() {
    if (canWalk(r, c + 1)) {
      c = c + 1;
    } else {
      return true;
    }
    return false;
  }

  bool walkN() {
    if (canWalk(r, c - 1)) {
      c = c - 1;
    } else {
      return true;
    }
    return false;
  }
}

class Question extends Map {
  int r;
  int c;
  String symbol = "Q";

  Question(this.r, this.c) : super(r, c, "Q");
}

class Bomb extends Map {
  Bomb(this.r, this.c) : super(r, c, "B");
  int r;
  int c;
  String symbol = "B";
}

class Game {
  int width;
  int height;
  late Player player;
  late Question question;
  late Bomb bomb;
  bool isFinished = false;
  List<Map> maps = [];
  int mapCount = 0;

  Game(this.width, this.height);

  void setPlayer(Player player) {
    this.player = player;
    addMap(player);
  }

  void setBomb(Bomb bomb) {
    this.bomb = bomb;
    addMap(bomb);
  }

  void addMap(Map map) {
    maps.add(map);
    mapCount++;
  }

  void setQuestiob(Question question) {
    this.question = question;
    addMap(question);
  }

  void showSymbol(int r, int c) {
    String symbol = '-';
    for (int i = 0; i < mapCount; i++) {
      if (maps[i].stayOn(r, c)) {
        symbol = maps[i].symbol;
      }
    }
    stdout.write(symbol);
  }

  void showMap() {
    for (int r = 0; r < height; r++) {
      for (int c = 0; c < width; c++) {
        showSymbol(r, c);
      }
      print('');
    }
  }

  void showBomb() {
    print(bomb.symbol);
  }

  void showPlayer() {
    print(player.symbol);
  }

  bool isBomb(int r, int c) {
    return bomb.stayOn(r, c);
  }

  bool inMap(int r, int c) {
    return (r >= 0 && r < width) && (c >= 0 && c < height);
  }

  bool isQuestion(int r, int c) {
    for (int o = 0; o < mapCount; o++) {
      if (maps[o] is Question && maps[o].stayOn(r, c)) {
        return true;
      }
    }
    return false;
  }
}

void main(List<String> arguments) {
  print("Welcome to Question or Bomb");
  Game game = Game(8, 8);
  Player player = Player(0, 0, game);
  game.addMap(new Question(5, 6));
  game.addMap(new Question(3, 2));
  Bomb bomb = Bomb(1, 6);
  Bomb bomb1 = Bomb(0, 2);
  Bomb bomb2 = Bomb(1, 8);
  Bomb bomb3 = Bomb(2, 0);
  Bomb bomb4 = Bomb(2, 4);
  Bomb bomb5 = Bomb(3, 7);
  Bomb bomb6 = Bomb(3, 3);
  Bomb bomb7 = Bomb(4, 5);
  Bomb bomb8 = Bomb(4, 0);
  Bomb bomb9 = Bomb(5, 2);
  Bomb bomb10 = Bomb(6, 7);
  Bomb bomb11 = Bomb(7, 4);

  game.setBomb(bomb);
  game.setBomb(bomb1);
  game.setBomb(bomb2);
  game.setBomb(bomb3);
  game.setBomb(bomb4);
  game.setBomb(bomb5);
  game.setBomb(bomb6);
  game.setBomb(bomb7);
  game.setBomb(bomb8);
  game.setBomb(bomb9);
  game.setBomb(bomb10);
  game.setBomb(bomb11);
  game.setPlayer(player);
  while (!game.isFinished) {
    game.showMap();
    String direction = stdin.readLineSync()!;
    if (direction == 'q') {
      print("Exit game.");
      break;
    }
    player.walk(direction);
  }
}
